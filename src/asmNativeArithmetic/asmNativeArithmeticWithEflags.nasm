;; Copyright (c) 2016, Jean-Baptiste Laurent
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are met:
;;
;; 1. Redistributions of source code must retain the above copyright notice, this
;;    list of conditions and the following disclaimer.
;; 2. Redistributions in binary form must reproduce the above copyright notice,
;;    this list of conditions and the following disclaimer in the documentation
;;    and/or other materials provided with the distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
;; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
;; ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;
;; The views and conclusions contained in the software and documentation are those
;; of the authors and should not be interpreted as representing official policies,
;; either expressed or implied, of the Barghest Project.
;; @brief Arithmetic function (add/sub/mul) design to return the overflow
;;
;; Perform an operation between a and b, and store the result in dst.
;; Return true if an overflow has occured
;; Prototype example ==> bool nativeAddInt(T a, T b, T *dst);
;;

;; Note: (file logic) (TODO: refaire la doc)
    ;; - The C/C++ code call one those wrapper
    ;; 1/ Init operation (parameter and volatil registers backup)
    ;; 2/ Init operation operand
    ;; 3/ Perform the operation (add/sub/mul/etc...)
    ;; 4/ Save eflags
    ;; 5/ Save operation result, remainder and high if not NULL
    ;; 6/ Clean stack
    ;; 7/ return ax (eflags:16)

    ;; The code is almost the same in 8/16/32/64b, thats why we use looping macro to generate it.
    ;; --> See DEFINE_LABEL
    ;; INIT_OP_[0-9]+ is a generic way to generate code which perform an operation

    ;; Notes: Converting "a - b" to "a + -b" produce the same result but different eflags.
section .text

;;; ;;;;;;;;;;;;;;;;;;;;;
    %defstr proc_type    __OUTPUT_FORMAT__ ; win32/win64/elf32/elf64
    %substr proc_type_str    proc_type 4,2 ; "32" or "64"
    %deftok proc_type_val    proc_type_str ; 32 or 64 bits
    %defstr bin_format    __OUTPUT_FORMAT__
    %substr bin_format    bin_format 1,3 ; win or elf
    %define proc_size    (proc_type_val / 8) ; 4 or 8 bytes

    ; GET_ARGS(unsigned int arg_number) ; Return the argument Number "arg_number" starting at 1
    %define GET_STACK_ARGS_NO_DEREF(arg_nb) GET_ARGS_32_NO_DEREF(arg_nb)
    %define GET_STACK_ARGS(arg_nb) [GET_STACK_ARGS_NO_DEREF(arg_nb)]

    %define GET_ARGS_64(arg_nb) [GET_REG(bp, proc_type_val) - (proc_size * (arg_nb))]
    %define GET_ARGS_32(arg_nb) [GET_ARGS_32_NO_DEREF(arg_nb)]
    %define GET_ARGS_32_NO_DEREF(arg_nb) GET_REG(bp, proc_type_val) + (proc_size * (1 + arg_nb))
    %if proc_type_str = "64" ; Use stack copy
        %define GET_ARGS(arg_nb) GET_ARGS_64(arg_nb)
    %else            ; Use orignal stack argument
        %define GET_ARGS(arg_nb) GET_ARGS_32(arg_nb)
    %endif
    ;; Copy the given register in the local backup stack
    %define SET_ARGS(reg, arg_nb) mov [GET_REG(bp, proc_type_val) - (proc_size * (arg_nb))], reg

    %macro SET_MEM_ARGS_NO_BACKUP 2    ; GET_ARGS(size_t *mem, unsigned int arg_number)
        mov GET_REG(ax, proc_type_val), [%1]
        SET_ARGS(GET_REG(ax, proc_type_val), %2)
    %endmacro

    %macro SET_MEM_ARGS 2    ; GET_ARGS(size_t *mem, unsigned int arg_number)
        push GET_REG(ax, proc_type_val)
        SET_MEM_ARGS_NO_BACKUP(%1, %2)
        pop GET_REG(ax, proc_type_val)
    %endmacro

    %pathsearch reg_type.nasm.inc "reg_type.nasm.inc"
    %include reg_type.nasm.inc

    %if bin_format = "elf"
        %deftok _FUNCTION ':function'
    %else
        %deftok _FUNCTION ''
    %endif

    %if proc_type_str = "32"
        %define ITER_NB 3 ; Loop 3 time for code generation (generate code for type size of 8,16 and 32b)
    %else
        %define ITER_NB 4 ; Loop 4 time for code generation (generate code for type size of 8,16,32 and 64b)
    %endif

    %macro  BACKUP_32b_VOLATILE_REGISTER 1
        ;; Prolog
        push GET_REG(bp, proc_type_val)
        mov GET_REG(bp, proc_type_val), GET_REG(sp, proc_type_val)
        %if proc_type_str = "32" ; Backup volatils register
            ;; All registers on the Intel386 are global and thus visible to both a calling and a
            ;; called function. Registers %ebp, %ebx, %edi, %esi, and %esp ‘‘belong’’ to the cal-
            ;; ling function. In other words, a called function must preserve these registers’
            ;; values for its caller.
            ;; push GET_REG(bx, proc_type_val)
            push GET_REG(di, proc_type_val)
            push GET_REG(si, proc_type_val)
        %else
            sub GET_REG(sp, proc_type_val), 5 * proc_size ; Parameter copy (Reg -> stack)
        %endif
        jmp %1
    %endmacro

;;; ;;;;;;;;;;;;;;;;;;;;;

    %macro  _CLEAR_EFLAGS 0                ; Reset all 6 arithmetics flags
        pushf                    ; get eflags
        and [GET_REG(sp, proc_type_val)], word ~0x8D5 ; 0b1000 11010101 (clear CF/PF/AF/ZF/SF/OF)
        popf                    ; set eflags
    %endmacro

    %macro _INIT_OP 2    ; _INIT_OP(op, reg_size (Generic arithmetic macro))
        mov GET_REG(ax, %2), GET_ARGS(1)
        %1 GET_REG(ax, %2), GET_SIZE_QUALIFIER(%2) GET_ARGS(2)
        pushf        ; Save eflags
        mov GET_REG(di, proc_type_val), GET_ARGS(3)
        test GET_REG(di, proc_type_val), GET_REG(di, proc_type_val)
        jz .skip_result
        mov [GET_REG(di, proc_type_val)], GET_REG(ax, %2) ; Store result
        .skip_result:
        ;; jmp _ret_eflags
    %endmacro

    ; "mul" only support 1 the operand form
    %macro  _INIT_OP_MULT_HIGH 2 ; GET_ARGS(4):GET_ARGS(3) = GET_ARGS(1) * GET_ARGS(2)
        _INIT_OP_MULT %1, %2
        mov GET_REG(si, proc_type_val), GET_ARGS(4)
        test GET_REG(si, proc_type_val), GET_REG(si, proc_type_val)
        jz .skip_result_high        ; NULL checking
        %if %2 = 8            ; store high part
            mov [GET_REG(si, proc_type_val)], ah
        %else        ; 16/32/64
            mov [GET_REG(si, proc_type_val)], GET_REG(dx, %2)
        %endif
        .skip_result_high:
    %endmacro

    ;; Specialize case for INIT_OP -> _INIT_OP_MULT(op, reg_size)
    %macro  _INIT_OP_MULT 2    ; imul r8/rm8 is not supported (have to use the 1 operand instruction)
        mov GET_REG(ax, %2), GET_ARGS(1)
        %1 GET_SIZE_QUALIFIER(%2) GET_ARGS(2)    ; multiplication
        pushf                    ; Save eflags
        mov GET_REG(di, proc_type_val), GET_ARGS(3)
        test GET_REG(di, proc_type_val), GET_REG(di, proc_type_val)
        jz .skip_result
        mov [GET_REG(di, proc_type_val)], GET_REG(ax, %2) ; store low part
        .skip_result:
    %endmacro

    %macro  _INIT_OP_DIV_HIGH 2 ;; _INIT_OP_DIV_HIGH(op, reg_size)
        mov GET_REG(ax, %2), GET_ARGS(1)     ;; Setting low part
        %if %2 = 8                ;; Setting High part
            mov cl, GET_ARGS(5)        ; Can't move 8b value in high part in 64b arch
            mov ah, cl
            %define _div_high_reg    ah
        %else
            mov GET_REG(dx, %2), GET_ARGS(5)
            %define _div_high_reg    GET_REG(dx, %2)
        %endif
        _INIT_OP_DIV_STORE %1, %2
    %endmacro

    ;; Specialize case for INIT_OP; TODO: Faire des if pour gérer le #DE si -min/-1 est detecte
    %macro  _INIT_OP_DIV 2 ;; _INIT_OP_DIV(op, reg_size)
        %defstr _div_op_str %1
        mov GET_REG(ax, %2), GET_ARGS(1)     ;; Setting low part
        %if %2 = 8
            %define _div_high_reg    ah
            %if _div_op_str = "idiv"    ;; Setting High part
                mov dl, GET_ARGS(1)
                sar dl, 0x7 ; Use DL because we can't shift high ah register
                mov _div_high_reg, dl
            %else    ; _div_op_str = "div"
                mov _div_high_reg, byte 0
            %endif
        %else        ; %2 = 16/32/64
            %define _div_high_reg   GET_REG(dx, %2)
            %if _div_op_str = "idiv"
                mov _div_high_reg, GET_ARGS(1)
                sar _div_high_reg, %2 - 1
            %else    ; _div_op_str = "div"
                mov _div_high_reg, 0
            %endif
        %endif
        _INIT_OP_DIV_STORE %1, %2
    %endmacro

    ;; Perform the division and store the quotient and the reminder in the
    ;; corresponding pointer. (Used in the division and the high division)
    %macro _INIT_OP_DIV_STORE 2
        %1 GET_SIZE_QUALIFIER(%2) GET_ARGS(2)
        pushf                        ; Save eflags
        mov GET_REG(di, proc_type_val), GET_ARGS(3) ; quotient_ptr
        mov GET_REG(si, proc_type_val), GET_ARGS(4) ; remainder_ptr
        test GET_REG(di, proc_type_val), GET_REG(di, proc_type_val)
        jz .skip_quotient
        mov [GET_REG(di, proc_type_val)], GET_REG(ax, %2)
        .skip_quotient:
        test GET_REG(si, proc_type_val), GET_REG(si, proc_type_val)
        jz .skip_remainder
        mov [GET_REG(si, proc_type_val)], _div_high_reg
        .skip_remainder:
    %endmacro

    ;; Specialize case for INIT_OP (left and right shift)
    %macro  _INIT_OP_SHIFT 2 ; _INIT_OP_SHIFT(op, reg_size)
        mov GET_REG(ax, %2), GET_ARGS(1)
        mov GET_REG(cx, %2), GET_ARGS(2)
        _CLEAR_EFLAGS    ; Clear because a shift of '0' keeps flags unchanged
        %1 GET_REG(ax, %2), cl
        pushf
        mov GET_REG(di, proc_type_val), GET_ARGS(3)
        test GET_REG(di, proc_type_val), GET_REG(di, proc_type_val)
        jz .skip_result
        mov [GET_REG(di, proc_type_val)], GET_REG(ax, %2)
        .skip_result:
    %endmacro

    %macro  INIT_OP 3    ; Init arithmetic parameters on 32b/64b and linux/windows
        ;; sub GET_REG(sp, proc_type_val), byte 8 ; stack place for mult/div high part storage
        %define op %1
        %defstr str_op %1
        %if bin_format = "elf" && proc_type_str = "64" ;; `RDI', `RSI', `RDX', `RCX', `R8', and `R9'
            SET_ARGS(GET_REG(di, proc_type_val), 1)
            SET_ARGS(GET_REG(si, proc_type_val), 2)
            SET_ARGS(GET_REG(dx, proc_type_val), 3)
            SET_ARGS(GET_REG(cx, proc_type_val), 4)
            SET_ARGS(GET_REG(r8, proc_type_val), 5)

        %elif bin_format = "win" && proc_type_str = "64" ;; `RCX', `RDX', `R8', `R9', Stack...
            SET_ARGS(GET_REG(cx, proc_type_val), 1)
            SET_ARGS(GET_REG(dx, proc_type_val), 2)
            SET_ARGS(GET_REG(r8, proc_type_val), 3)
            SET_ARGS(GET_REG(r9, proc_type_val), 4)
            SET_MEM_ARGS_NO_BACKUP GET_STACK_ARGS_NO_DEREF(1), 6
        %elif (bin_format = "win" || bin_format = "elf") && proc_type_str = "32"
            ;; Getting parameters directly on stack
        %else
            %fatal Platform [__OUTPUT_FORMAT__] not supported (available: win32/win64 or elf32/elf64) ; No need to be quoted
        %endif

        ;; For the multiplication instruction we generate the 1 operand form
        ;; because the 8b version of imul and 8/16/32/64b version of mul does not support 2 operand
        %if str_op = "mul" || str_op = "imul"
            %if %3 = "low"
                _INIT_OP_MULT op, %2
            %else    ; "high"
                _INIT_OP_MULT_HIGH op, %2
            %endif
        %elif str_op = "div" || str_op = "idiv"
            %if %3 = "low"
                _INIT_OP_DIV op, %2
            %else    ; "high"
                _INIT_OP_DIV_HIGH op, %2
            %endif
        %elif str_op = "shl" || str_op = "shr" || str_op = "sal" || str_op = "sar"
            _INIT_OP_SHIFT op, %2
        %else
            _INIT_OP op, %2
        %endif
        ;; add GET_REG(sp, proc_type_val), byte 8
        jmp _ret_eflags
    %endmacro

;;; ;;;;;;;;;;;;;;;;;;;;;

    ;; DEFINE_LABEL(iteration_nb, sub_code_label, add_code_label,
    ;;         signed_mult_code_label, unsigned_muld_code_label,
    ;;         left_shift_label, signed_right_shift_label, unsigned_right_shift_label,
    ;;         etc...)
    ;; The iteration_nb param is used for 32b/16b code to not use register that does not exist (ex: rdi)
    ;; For example, if the value is 1, only the 8b label and code will be defined (8),
    ;;              if the value is 3, only the 8b/16b/32b label and code will be defined (8/16/32)
    %macro DEFINE_LABEL 17
        %assign i 8
        %rep    %1
            ;; Label declaration
            global nativeAddUInt%+i%+_FUNCTION
            global nativeAddInt%+i%+_FUNCTION
            global nativeSubUInt%+i%+_FUNCTION
            global nativeSubInt%+i%+_FUNCTION

            global nativeMultInt%+i%+_FUNCTION
            global nativeMultUInt%+i%+_FUNCTION
            global nativeMultHighInt%+i%+_FUNCTION
            global nativeMultHighUInt%+i%+_FUNCTION

            global nativeLShiftInt%+i%+_FUNCTION
            global nativeLShiftUInt%+i%+_FUNCTION

            global nativeRShiftInt%+i%+_FUNCTION
            global nativeRShiftUInt%+i%+_FUNCTION

            global nativeBitAndInt%+i%+_FUNCTION
            global nativeBitAndUInt%+i%+_FUNCTION

            global nativeBitOrInt%+i%+_FUNCTION
            global nativeBitOrUInt%+i%+_FUNCTION

            global nativeBitXorInt%+i%+_FUNCTION
            global nativeBitXorUInt%+i%+_FUNCTION

            global nativeDivInt%+i%+_FUNCTION
            global nativeDivUInt%+i%+_FUNCTION
            global nativeDivHighInt%+i%+_FUNCTION
            global nativeDivHighUInt%+i%+_FUNCTION

            ;; Unsigned substraction label
            nativeSubUInt%+i:    BACKUP_32b_VOLATILE_REGISTER %2%+i
            ;; Signed substraction label
            nativeSubInt%+i:    BACKUP_32b_VOLATILE_REGISTER %2%+i
            ;; Unsigned addition label
            nativeAddUInt%+i:    BACKUP_32b_VOLATILE_REGISTER %3%+i
            ;; Signed addition label
            nativeAddInt%+i:    BACKUP_32b_VOLATILE_REGISTER %3%+i

            ;; Signed multiplication label
            nativeMultInt%+i:    BACKUP_32b_VOLATILE_REGISTER %4%+i
            ;; Unsigned multiplication label
            nativeMultUInt%+i:    BACKUP_32b_VOLATILE_REGISTER %5%+i

            ;; Signed multiplication label
            nativeMultHighInt%+i:    BACKUP_32b_VOLATILE_REGISTER %6%+i
            ;; Unsigned multiplication label
            nativeMultHighUInt%+i: BACKUP_32b_VOLATILE_REGISTER %7%+i

            ;; Signed and unsigned left shift (logical/arithmetical)
            nativeLShiftInt%+i:
            nativeLShiftUInt%+i:    BACKUP_32b_VOLATILE_REGISTER %8%+i
            ;; Signed right shift
            nativeRShiftInt%+i:    BACKUP_32b_VOLATILE_REGISTER %9%+i
            ;; Unsigned right shift
            nativeRShiftUInt%+i:    BACKUP_32b_VOLATILE_REGISTER %10%+i

            nativeBitAndInt%+i:
            nativeBitAndUInt%+i:    BACKUP_32b_VOLATILE_REGISTER %11%+i

            nativeBitOrInt%+i:
            nativeBitOrUInt%+i:    BACKUP_32b_VOLATILE_REGISTER %12%+i

            nativeBitXorInt%+i:
            nativeBitXorUInt%+i:    BACKUP_32b_VOLATILE_REGISTER %13%+i

            ;; Signed and unsigned division (with and whitout high part)
            nativeDivInt%+i:    BACKUP_32b_VOLATILE_REGISTER %14%+i
            nativeDivUInt%+i:    BACKUP_32b_VOLATILE_REGISTER %15%+i
            nativeDivHighInt%+i:    BACKUP_32b_VOLATILE_REGISTER %16%+i
            nativeDivHighUInt%+i:    BACKUP_32b_VOLATILE_REGISTER %17%+i

            ;; - Sub functions (signed and unsigned)
            %2%+i:    INIT_OP sub, i, ""
            ;; + Add functions (signed and unsigned)
            %3%+i:    INIT_OP add, i, ""

            ;; * IMult functions (signed)
            %4%+i:    INIT_OP imul, i, "low"
            ;; * Mult functions (unsigned)
            %5%+i:    INIT_OP mul, i, "low"
            ;; * IMult functions with "high" returned (signed)
            %6%+i:    INIT_OP imul, i, "high"
            ;; * Mult functions with "high" returned (unsigned)
            %7%+i:    INIT_OP mul, i, "high"

            ;; << logical/arithmetical left shift (signed and unsigned) (shl/sal)
            %8%+i:    INIT_OP shl, i, ""
            ;; >> arithmetical right shift (signed) (sar)
            %9%+i:    INIT_OP sar, i, ""
            ;; >> logical right shift (unsigned) (shr)
            %10%+i:    INIT_OP shr, i, ""

            ;; & binary and (signed and unsigned)
            %11%+i:    INIT_OP and, i, ""
            ;; | binary or (signed and unsigned)
            %12%+i:    INIT_OP or, i, ""
            ;; ^ binary xor (signed and unsigned)
            %13%+i:    INIT_OP xor, i, ""

            ;; * IDiv functions (signed)
            %14%+i:    INIT_OP idiv, i, "low"
            ;; * Div functions (unsigned)
            %15%+i:    INIT_OP div, i, "low"
            ;; * IDiv functions with "high" returned (signed)
            %16%+i:    INIT_OP idiv, i, "high"
            ;; * Div functions with "high" returned (unsigned)
            %17%+i:    INIT_OP div, i, "high"

        %assign i i*2 ;; i = {8, 16, 32, 64}
        %endrep
    %endmacro ;; DEFINE_LABEL

DEFINE_LABEL ITER_NB, _performIntegerSubstract, _performIntegerAddition,             \
    _performSignedIntegerMult, _performUnsignedIntegerMult,                    \
    _performSignedIntegerMultHigh, _performUnsignedIntegerMultHigh,                \
    _performIntegerLShift, _performSignedIntegerRShift, _performUnsignedIntegerRShift,    \
    _performBitAnd, _performBitOr, _performBitXor,                        \
    _performSignedIntegerDiv, _performUnsignedIntegerDiv,                    \
    _performSignedIntegerDivHigh, _performUnsignedIntegerDivHigh

    ;; the return value is stored in 'rax' on both windows and unix
_ret_eflags:
    ;; pushf ; Done right after the arithmetic instruction
    movzx GET_REG(ax, proc_type_val), word [GET_REG(sp, proc_type_val)]
    popf

%if proc_type_str = "32"
    pop GET_REG(si, proc_type_val)
    pop GET_REG(di, proc_type_val)
    ;; pop GET_REG(bx, proc_type_val) ; rbx is unused here, uncomment if necessary
%endif
    leave            ; Epilog
    ret
