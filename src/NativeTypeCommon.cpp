// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
///
/// @brief Implementation of the class NativeTypeCommon
///

#include "NativeTypeCommon.hh"

namespace nativeType
{

  //! Return the overflow status
  bool        NativeTypeCommon::getOverflow() const
  {
    return (this->_overflow);
  }
  //! Set the overflow flag to "value"
  void        NativeTypeCommon::setOverflow(bool value)
  {
    this->_overflow = value;
  }
  //! Reset the overflow flag
  void        NativeTypeCommon::clrOverflow()
  {
    this->setOverflow(false);
  }

  //! This function return and reset the overflow status.
  bool        NativeTypeCommon::getAndClrOverflow()
  {
    bool    flag;

    flag = this->getOverflow();
    this->clrOverflow();
    return (flag);
  }

  //! Set the eflags stored
  void        NativeTypeCommon::setEflags(uint16_t f)
  {
    this->_eflags.word = f;
  }

  void        NativeTypeCommon::setEflags(t_eflags const &f)
  {
    this->_eflags.word = f.word;
  }

  uint16_t    NativeTypeCommon::getEflags(void) const
  {
    return (this->_eflags.word);
  }

  t_eflags const    &NativeTypeCommon::getEflagsStruct(void) const
  {
    return (this->_eflags);
  }

  //! Reset the overflow flag
  void        NativeTypeCommon::clrEflags(void)
  {
    this->_eflags.word = 0;
  }

  //! Default constructor
  NativeTypeCommon::NativeTypeCommon()
  {
    this->clrOverflow();
    this->clrEflags();
  }
} // !namespace nativeType
