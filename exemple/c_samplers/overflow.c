/*!
** Copyright (c) 2016, Jean-Baptiste Laurent
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice, this
**    list of conditions and the following disclaimer.
** 2. Redistributions in binary form must reproduce the above copyright notice,
**    this list of conditions and the following disclaimer in the documentation
**    and/or other materials provided with the distribution.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
** ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** The views and conclusions contained in the software and documentation are those
** of the authors and should not be interpreted as representing official policies,
** either expressed or implied, of the Barghest Project.
**
** @brief A simple example of how to use nativeTypeArithmetic in C.
*/

#include <stdio.h>    /* printf() */
#include "cCppNativeArithmeticWithEflags.hh"

/*!
** Use the SF (Sign Flag) in order to tell if the result
** is positive or negative.
*/
void        isNeg(t_eflags const *f)
{
  if (f->s_eflags.SF == 1)
    printf("The result is negative\n");
  else
    printf("The result is positive\n");
}

/*!
** Perform a sign addition and check for an overflow if any.
*/
void        signAddOverflow(int a, int b)
{
  int        result;
  t_eflags    f;

  f.word = nativeAddInt32(a, b, &result);
  if (f.s_eflags.OF == 1) /* OF for signed overflow */
    printf("Error: Sign Overflow detected (%d + %d = %d) ==> ", a, b, result);
  else
    printf("Success: %d + %d = %d ==> ", a, b, result);
  isNeg(&f);
}

/*!
** Perform an unsign multiplication and check for an overflow if any.
*/
void        unsignMultOverflow(unsigned int a, unsigned int b)
{
  unsigned int    result;
  t_eflags    f;

  f.word = nativeMultUInt32(a, b, &result);
  if (f.s_eflags.CF == 1) /* CF for unsigned overflow */
    printf("Error: Unsigned Overflow detected (%u * %u = %u)\n", a, b, result);
  else
    printf("Success: %u * %u = %u\n", a, b, result);
}

/*!
** An example of code which perform an operation between 'a' and 'b',
** store the result in 'result' and print a message on the overflow.
*/
int        main()
{
  printf("==== Using signAddOverflow ======\n");
  signAddOverflow(5, 6);
  signAddOverflow(-1552, 6);
  signAddOverflow(-2147483648, -1);
  signAddOverflow(2147483647, 1);
  /**/
  printf("\n==== Using unsignMultOverflow ======\n");
  unsignMultOverflow(15, 3);
  unsignMultOverflow(50000000, 4294967);
  printf("\n==== End ======\n");
  return (0);
}
