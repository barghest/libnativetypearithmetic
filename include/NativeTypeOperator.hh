// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
///
/// @brief Operator header for the class NativeType
///

#ifndef NATIVE_TYPE_OPERATOR_HH_
# define NATIVE_TYPE_OPERATOR_HH_

# include "NativeType.hh"

namespace nativeType
{
  template <typename T>
  NativeType<T>    PerformOperation(NativeType<T> const &a,
                                    NativeType<T> const &b,
                                    uint16_t (*arithmetic_fct)(T const, T const, T *));

  template <typename T> NativeType<T>    operator+(NativeType<T> const &a,
                                                   NativeType<T> const &b);
  template <typename T> NativeType<T>    operator-(NativeType<T> const &a,
                                                   NativeType<T> const &b);
  template <typename T> NativeType<T>    operator*(NativeType<T> const &a,
                                                   NativeType<T> const &b);
  template <typename T> NativeType<T>    operator<<(NativeType<T> const &a,
                                                    NativeType<T> const &b);
  template <typename T> NativeType<T>    operator>>(NativeType<T> const &a,
                                                    NativeType<T> const &b);
  template <typename T> NativeType<T>    operator^(NativeType<T> const &a,
                                                   NativeType<T> const &b);
  template <typename T> NativeType<T>    operator|(NativeType<T> const &a,
                                                   NativeType<T> const &b);
  template <typename T> NativeType<T>    operator&(NativeType<T> const &a,
                                                   NativeType<T> const &b);
  template <typename T> NativeType<T>    operator/(NativeType<T> const &a,
                                                   NativeType<T> const &b);

} // !namespace nativeType

#endif /* ! NATIVE_TYPE_OPERATOR_HH_ */
