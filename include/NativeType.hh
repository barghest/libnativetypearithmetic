// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
// @brief NativeType class Description
//

#ifndef NATIVE_TYPE_HH_
# define NATIVE_TYPE_HH_

// # include <type_traits>

//! Include the parent class
# include "NativeTypeCommon.hh"

namespace nativeType
{

//! @brief NativeType is a wrapper of a native type.
//! This class is used to keep track of an overflow during interval arithmetic in boost::interval.
//! Exemple when using the addition: (int8_t)[-1, 127] + 1 => (int8_t)[0, -128]. There is an overflow.
//! Note: methods not depending on type T have been moved on parent class NativeTypeCommon
//! to speed up the compilation process.
  template <typename T>
  class NativeType : public NativeTypeCommon
  {
    //! Static Assert if a type is not an arithmetic type
    //! (can't use to allow standard C++ compatibility (and not only C++11))
    // static_assert(std::is_arithmetic<T>::value, "T must be an arithmetic type");
    // static_assert(std::is_same<T, int8_t>::value || std::is_same<T, uint8_t>::value
    //           || std::is_same<T, int16_t>::value || std::is_same<T, uint16_t>::value
    //           || std::is_same<T, int32_t>::value || std::is_same<T, uint32_t>::value
    //           || std::is_same<T, int64_t>::value || std::is_same<T, uint64_t>::value
    //           , "T type is not supported");
  public:

    //! Default constructor
    NativeType() : NativeTypeCommon(), _value(0)
    {
    }

    //! Value constructor
    NativeType(T const &value) : NativeTypeCommon(), _value(value)
    {
    }

    //! Cast operator
    inline operator T() const {return (getValue());}

    //! Asignation operator
    inline NativeType<T> operator=(T const &value) {setValue(value);return (_value);}

    //! Return the value of the native type T
    inline T getValue(void) const {return (_value);}
    //! Set the value of the native type T using "value"
    inline void setValue(T const value) {_value = value;}
  private:
    //! The real value of the Type overloaded
    T    _value;
  }; // !class NativeType

} // !namespace nativeType

# include "NativeTypeOperator.hh"

#endif /* !NATIVE_TYPE_HH_ */
