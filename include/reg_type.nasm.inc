;; Copyright (c) 2016, Jean-Baptiste Laurent
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are met:
;;
;; 1. Redistributions of source code must retain the above copyright notice, this
;;    list of conditions and the following disclaimer.
;; 2. Redistributions in binary form must reproduce the above copyright notice,
;;    this list of conditions and the following disclaimer in the documentation
;;    and/or other materials provided with the distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
;; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
;; ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;
;; The views and conclusions contained in the software and documentation are those
;; of the authors and should not be interpreted as representing official policies,
;; either expressed or implied, of the Barghest Project.
;;
;; @brief typedef of register (similar to stdint.h)
;;

%ifndef REG_TYPE_INC_
 %define REG_TYPE_INC_

    %defstr __proc_type  __OUTPUT_FORMAT__ ; win32/win64/elf32/elf64
    %substr __proc_type_str __proc_type 4,2 ; "32" or "64"
    %deftok __proc_type_val __proc_type_str ; 32 or 64

    %define ax8_t al
    %define ax16_t ax
    %define ax32_t eax
    %define ax64_t rax

    %define bx8_t bl
    %define bx16_t bx
    %define bx32_t ebx
    %define bx64_t rbx

    %define cx8_t cl
    %define cx16_t cx
    %define cx32_t ecx
    %define cx64_t rcx

    %define dx8_t dl
    %define dx16_t dx
    %define dx32_t edx
    %define dx64_t rdx

    %define di8_t dil
    %define di16_t di
    %define di32_t edi
    %define di64_t rdi

    %define si8_t sil
    %define si16_t si
    %define si32_t esi
    %define si64_t rsi

    %define r88_t r8b
    %define r816_t r8w
    %if __proc_type_str = "32"
        %define r832_t r8
    %else
        %define r832_t r8d
        %define r864_t r8
    %endif


    %define r98_t r9b
    %define r916_t r9w
    %if __proc_type_str = "32"
        %define r932_t r9
    %else
        %define r932_t r8d
        %define r964_t r9
    %endif

    %define bp32_t ebp
    %define bp64_t rbp

    %define sp32_t esp
    %define sp64_t rsp

%define GET_REG(base_reg, size) base_reg%+size%+_t

    %define qualifier_size_8    byte
    %define qualifier_size_16    word
    %define qualifier_size_32    dword
    %define qualifier_size_64    qword

%define GET_SIZE_QUALIFIER(size) qualifier_size_%+size
    ;; Exemple: GET_REG(di, 8) -->  di8_t  --> dil
    ;; Exemple: GET_REG(ax, 32) --> ax32_t --> eax
%endif     ;; ! REG_TYPE_INC_
